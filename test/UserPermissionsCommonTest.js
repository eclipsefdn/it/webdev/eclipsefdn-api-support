/** **************************************************************
 Copyright (C) 2023 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/
var chai = require('chai');
var expect = chai.expect;

const { castJsonToOverrideFormat } = require('../src/UserPermissions/UserPermissionsCommon.js');
const { ZodError } = require('zod');
// pull in sample JSON files for some of the more manual tests
const sampleJson = JSON.stringify(require('./perms/test.json'));
const badJson = JSON.stringify(require('./perms/invalid_format.json'));

describe('UserPermissionsCommon', function() {
  describe('castJsonToOverrideFormat', function() {
    describe('success', function() {
      it('should process matching schema', function() {
        // used in setup, but being explicit about it here
        expect(castJsonToOverrideFormat(sampleJson)).to.not.be.undefined;
      });
      it('should convert datestring to date', function() {
        // used in setup, but being explicit about it here
        expect(castJsonToOverrideFormat(sampleJson)).to.have.nested.property('overrides.[0].expiry').and.to.be.instanceOf(Date);
      });
    });
    describe('failure', function() {
      it('should throw errors on bad formats', function() {
        expect(() => castJsonToOverrideFormat(badJson)).to.throw(ZodError);
      });
    });
  });
});