# Notices for eclipsefdn-api-support

This content is produced and maintained by the Eclipse Foundation. Trademarks are the property of their respective owners.

* Project home: https://gitlab.eclipse.org/eclipsefdn/it/webdev/eclipsefdn-api-support

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://gitlab.eclipse.org/eclipsefdn/it/webdev/eclipsefdn-api-support

## Third-party Content

### Yargs

* License: MIT License (MIT)

### Decamelize

* License: MIT License (MIT)

### openapi-schema-to-json-schema

* License: MIT License (MIT)

### js-yaml

* License: MIT License (MIT)

### json-ref-resolver

* License: Unlicensed
