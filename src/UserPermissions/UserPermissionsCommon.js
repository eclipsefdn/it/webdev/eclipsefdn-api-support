/** **************************************************************
 Copyright (C) 2023 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/
const { z } = require('zod');
const SUPPORTED_PERMISSION_LEVELS = ['READ', 'TRIAGE', 'WRITE', 'MAINTAIN'];

const USER_PERMISSIONS_SCHEMA = z.object({
  overrides: z.array(
    z.object({
      name: z.string(),
      permission: z.enum(SUPPORTED_PERMISSION_LEVELS),
      expiry: z.coerce.date(),
      target_projects: z.array(z.string()),
    })
  ),
  excluded: z.array(z.string()),
});

function castJsonToOverrideFormat(jsonString) {
  return USER_PERMISSIONS_SCHEMA.parse(JSON.parse(jsonString));
}

module.exports.USER_PERMISSIONS_SCHEMA = USER_PERMISSIONS_SCHEMA;
module.exports.castJsonToOverrideFormat = castJsonToOverrideFormat;
