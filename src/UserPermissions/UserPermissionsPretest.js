/** **************************************************************
 Copyright (C) 2023 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/
const argv = require('yargs')
  .usage('Usage: $0 [options]')
  .example('$0', '')
  .option('f', {
    alias: 'file',
    description: 'The location of the JSON file to test. Should be readable by the current user',
  }).argv;

const fs = require('fs');
const { castJsonToOverrideFormat } = require('UserPermissionsCommon.js');
// attempt to parse the passed file as override JSON
try {
  const out = castJsonToOverrideFormat(fs.readFileSync(argv.f, { encoding: 'UTF-8' }));
  // let the user know what we found generally.
  console.log(`Successfully parsed ${out.overrides.length} overrides and ${out.excluded.length} exclusions`);
} catch (e) {
  console.log(`Error encountered while processing JSON at ${argv.f}: ` + e);
  // error out if bad
  process.exit(1);
}
