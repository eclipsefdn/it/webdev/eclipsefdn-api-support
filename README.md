# eclipsefdn-api-support

Package for scripts related to supporting the Eclipse Foundation API projects.

## Getting started

Dependencies:

| Program | Version |
|---------|---------|
| node.js | ~ 12.22.0 |
| npm | ~ 7.11 |

## Usage

Below are example usages of the dependencies within the scope of developing an API.

### Previewing OpenAPI specs

```
npx @redocly/openapi-cli preview-docs spec/openapi.yaml -p 8097
```

### Linting/testing OpenAPI specs

```
npx @redocly/openapi-cli lint spec/openapi.yaml
```

### Compiling JSON Schema from OpenAPI

```
node node_modules/eclipsefdn-api-support/src/openapi2schema.js -s spec/openapi.yaml -t src/test/resources
```

## Bugs and Feature Requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/webdev/eclipsefdn-api-support/-/issues/new).

## Author

**Martin Lowe (Eclipse Foundation)**

- <https://github.com/autumnfound>

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright and License

Copyright 2022 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [eclipsefdn-api-support authors](https://gitlab.eclipse.org/eclipsefdn/it/webdev/eclipsefdn-api-support/-/graphs/main). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/webdev/eclipsefdn-api-support/-/blob/main/LICENSE).
